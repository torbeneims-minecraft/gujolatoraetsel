{Item:{id:"minecraft:diamond_pickaxe",Damage:1,Count:1b,tag:{CanDestroy:["minecraft:concrete"],HideFlags:1,display:{Name:"Geladene Diamantene Spitzhacke",Lore:[]},ench:[{id:34,lvl:1}]}}}

{Item:{id:"minecraft:diamond_pickaxe",Count:1b,tag:{Unbreakable:1b,display:{Name:"Diamantene Spitzhacke",Lore:["Energie:","░░░░░░░░░░░░░░░░░░░░","0/1000 EU"]}}}}
{Item:{tag:{CanDestroy:["minecraft:concrete"],HideFlags:1,display:{Name:"Geladene Diamantene Spitzhacke",Lore:["Energie:","████████████████████","1000/1000 EU"]},ench:[{id:34,lvl:1}]}}}

{Item:{id:"minecraft:wooden_button",Count:1b,tag:{display:{Name:"Uralter Knopf",Lore:["ᴇἦếᶴῶḽḝ:","≡╗╩≡╪╩╪≡╗≡╗╗≡╩╩╪≡╗╩≡","Ό҂ךּΩὈ╩ ỂШ"]}}}}
{Item:{id:"minecraft:stone_button",tag:{CanPlaceOn:["minecraft:wool"],display:{Name:"Geladener Knopf",Lore:["Energie:","████████████████████","1000/1000 EU"]},ench:[{id:34,lvl:1}],HideFlags:1}}}

Energie:
░░░░░░░░░░░░░░░░░░░░
████████████████████
0/1000 EU

