# particle mobSpell x y z R G B Speed Cnt=0 mode player
execute @e[tag=Checkpoint] ~ ~ ~ particle mobSpell ~ ~ ~ 0 1 0.85 0.8 0 normal 

execute @e[tag=Checkpoint] ~ ~ ~ scoreboard players tag @a[r=1] add eligibleCheckpoint {OnGround:1b}
execute @a[tag=eligibleCheckpoint] ~ ~ ~ detect ~ ~-1 ~ air * scoreboard players tag remove @s eligibleCheckpoint
execute @e[tag=Checkpoint] ~ ~ ~ spawnpoint @a[r=1,tag=eligibleCheckpoint] ~ ~ ~
execute @e[tag=Checkpoint] ~ ~ ~ title @a[r=1,tag=eligibleCheckpoint] actionbar [{"text":"► Checkpoint erreicht! ◄","color":"dark_aqua"}] 