scoreboard objectives remove success
scoreboard objectives remove reload
scoreboard objectives remove clock
scoreboard objectives add success dummy Success
scoreboard objectives add reload trigger Reload Status
scoreboard objectives add clock dummy Clock
scoreboard players enable @a reload