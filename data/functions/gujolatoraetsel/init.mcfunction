# 2017-06-27 17:40 - Torben
scoreboard objectives add success dummy Erfolg
scoreboard objectives add reload trigger Reload Status
scoreboard players enable @a reload
scoreboard objectives add builder trigger Bauteam
scoreboard players enable @a builder
scoreboard objectives add info trigger Info
scoreboard players enable @a info
scoreboard objectives add level dummy
scoreboard objectives add loaded_level dummy
scoreboard objectives add display_level dummy Level
scoreboard objectives setdisplay sidebar display_level
scoreboard objectives setdisplay list level
scoreboard objectives add deathRespawn dummy

scoreboard teams add aqua Aqua
scoreboard teams add black Schwarz
scoreboard teams add blue Blau
scoreboard teams add cyan Cyan
scoreboard teams add dark_blue Dunkelblau
scoreboard teams add gray Grau
scoreboard teams add green Grün
scoreboard teams add purple Lila
scoreboard teams add dark_red Dunkelrot
scoreboard teams add gold Gold
scoreboard teams add light_gray Hellgrau
scoreboard teams add lime Lime
scoreboard teams add magenta Magenta
scoreboard teams add red Rot
scoreboard teams add white Weiß
scoreboard teams add yellow Gelb

scoreboard teams add builder Bauteam

scoreboard teams option aqua color aqua
scoreboard teams option black color black
scoreboard teams option blue color blue
scoreboard teams option cyan color dark_aqua
scoreboard teams option dark_blue color dark_blue
scoreboard teams option gray color dark_gray
scoreboard teams option green color dark_green
scoreboard teams option purple color dark_purple
scoreboard teams option dark_red color dark_red
scoreboard teams option gold color gold
scoreboard teams option light_gray color gray
scoreboard teams option lime color green
scoreboard teams option magenta color light_purple
scoreboard teams option red color red
scoreboard teams option white color white
scoreboard teams option yellow color yellow

scoreboard teams option aqua seeFriendlyInvisibles false
scoreboard teams option black seeFriendlyInvisibles false
scoreboard teams option blue seeFriendlyInvisibles false
scoreboard teams option cyan seeFriendlyInvisibles false
scoreboard teams option dark_blue seeFriendlyInvisibles false
scoreboard teams option gray seeFriendlyInvisibles false
scoreboard teams option green seeFriendlyInvisibles false
scoreboard teams option purple seeFriendlyInvisibles false
scoreboard teams option dark_red seeFriendlyInvisibles false
scoreboard teams option gold seeFriendlyInvisibles false
scoreboard teams option light_gray seeFriendlyInvisibles false
scoreboard teams option lime seeFriendlyInvisibles false
scoreboard teams option magenta seeFriendlyInvisibles false
scoreboard teams option red seeFriendlyInvisibles false
scoreboard teams option white seeFriendlyInvisibles false
scoreboard teams option yellow seeFriendlyInvisibles false

scoreboard teams join aqua @e[tag=levelCreator,name=LevelCreatorAqua]
scoreboard teams join black @e[tag=levelCreator,name=LevelCreatorBlack]
scoreboard teams join blue @e[tag=levelCreator,name=LevelCreatorBlue]
scoreboard teams join cyan @e[tag=levelCreator,name=LevelCreatorCyan]
scoreboard teams join dark_blue @e[tag=levelCreator,name=LevelCreatorDark_blue]
scoreboard teams join gray @e[tag=levelCreator,name=LevelCreatorGray]
scoreboard teams join green @e[tag=levelCreator,name=LevelCreatorGreen]
scoreboard teams join purple @e[tag=levelCreator,name=LevelCreatorPurple]
scoreboard teams join dark_red @e[tag=levelCreator,name=LevelCreatorDark_red]
scoreboard teams join gold @e[tag=levelCreator,name=LevelCreatorGold]
scoreboard teams join light_gray @e[tag=levelCreator,name=LevelCreatorLight_gray]
scoreboard teams join lime @e[tag=levelCreator,name=LevelCreatorLime]
scoreboard teams join magenta @e[tag=levelCreator,name=LevelCreatorMagenta]
scoreboard teams join red @e[tag=levelCreator,name=LevelCreatorRed]
scoreboard teams join white @e[tag=levelCreator,name=LevelCreatorWhite]
scoreboard teams join yellow @e[tag=levelCreator,name=LevelCreatorYellow]

scoreboard teams join aqua @e[tag=levelRepresenter,name=Aqua]
scoreboard teams join black @e[tag=levelRepresenter,name=Schwarz]
scoreboard teams join blue @e[tag=levelRepresenter,name=Blau]
scoreboard teams join cyan @e[tag=levelRepresenter,name=Cyan]
scoreboard teams join dark_blue @e[tag=levelRepresenter,name=Dunkelblau]
scoreboard teams join gray @e[tag=levelRepresenter,name=Grau]
scoreboard teams join green @e[tag=levelRepresenter,name=Grün]
scoreboard teams join purple @e[tag=levelRepresenter,name=Lila]
scoreboard teams join dark_red @e[tag=levelRepresenter,name=Dunkelrot]
scoreboard teams join gold @e[tag=levelRepresenter,name=Gold]
scoreboard teams join light_gray @e[tag=levelRepresenter,name=Hellgrau]
scoreboard teams join lime @e[tag=levelRepresenter,name=Lime]
scoreboard teams join magenta @e[tag=levelRepresenter,name=Magenta]
scoreboard teams join red @e[tag=levelRepresenter,name=Rot]
scoreboard teams join white @e[tag=levelRepresenter,name=Weiß]
scoreboard teams join yellow @e[tag=levelRepresenter,name=Gelb]
