effect @a[score_level25-title_min=0,score_level25-title=0] blindness 36 0 true
title @a[score_level25-title_min=35,score_level25-title=35] title [{"text":"Eines Tages,","color":"green"}]
title @a[score_level25-title_min=70,score_level25-title=70] title [{"text":"als die Menschheit","color":"green"}]
title @a[score_level25-title_min=110,score_level25-title=110] title [{"text":"einen bedeutenden Schritt","color":"green"}]
title @a[score_level25-title_min=145,score_level25-title=145] title [{"text":"in die Zukunft wagte ","color":"green"}]
title @a[score_level25-title_min=185,score_level25-title=185] title [{"text":"und den ersten","color":"green"}]
title @a[score_level25-title_min=215,score_level25-title=215] title [{"text":"Menschen","color":"green"}]
title @a[score_level25-title_min=235,score_level25-title=235] title [{"text":"auf den Mars","color":"green"}]
title @a[score_level25-title_min=260,score_level25-title=260] title [{"text":"schickte,","color":"green"}]
title @a[score_level25-title_min=280,score_level25-title=280] title [{"text":"ereignete sich","color":"green"}]
title @a[score_level25-title_min=315,score_level25-title=315] title [{"text":"ein tragisches Unglück...","color":"green"}]
title @a[score_level25-title_min=365,score_level25-title=365] title [{"text":"Ein Stein","color":"green"}]
title @a[score_level25-title_min=392,score_level25-title=392] title [{"text":"schlug ein Loch","color":"green"}]
execute @a[c=1,score_level25-title_min=402,score_level25-title=402] ~ ~ ~ playsound entity.firework.blast ambient @s ~ ~-1 ~ 1 0.1
title @a[score_level25-title_min=442,score_level25-title=442] title [{"text":"in den Treibstofftank...","color":"green"}]
title @a[score_level25-title_min=467,score_level25-title=467] title [{"text":"Doch fanden","color":"green"}]
title @a[score_level25-title_min=502,score_level25-title=502] title [{"text":"die Wissenschaftler","color":"green"}]
title @a[score_level25-title_min=537,score_level25-title=537] title [{"text":"zu DEINEM Glück","color":"green"}]
title @a[score_level25-title_min=587,score_level25-title=587] title [{"text":"eine Treibstoffalternative...","color":"green"}]
title @a[score_level25-title_min=622,score_level25-title=622] title [{"text":"Nun liegt es an dir","color":"green"}]
title @a[score_level25-title_min=657,score_level25-title=657] title [{"text":"sie zu finden!","color":"green"}]
execute @a[score_level25-title_min=637,score_level25-title=637] ~ ~ ~ execute @e[c=1,r=5,tag=level25-button] ~ ~ ~ setblock ~ ~ ~ stone_button 3
#effect @a[score_level25-title_min=657,score_level25-title=657] blindness 1 255 true