
# Giving the effects
effect @s[type=Player] minecraft:instant_health 1 200 false
effect @s[type=Player] minecraft:saturation 5 200 true
effect @s[type=Player] minecraft:regeneration 10 255 true
effect @s[type=Player] minecraft:resistance 10 7 true

# Removing the old level
execute @s[tag=levelCreator] ~ ~ ~ fill ~ ~ ~ ~31 ~31 ~31 air
execute @s[tag=levelCreator] ~ ~ ~ fill ~ ~-2 ~ ~ ~ ~ air
execute @s[tag=levelCreator] ~ ~ ~ kill @e[dx=31,dy=18,dz=31,tag=!levelCreator,type=!Player]

clear @s[type=Player]
effect @s[type=Player] clear
title @s[type=Player] title title {"text":"Loading Level ..."}
tellraw @a {"text":"Generating a level. Expect lag for a short while."}

# Setting the structure block corresponding to the level 
execute @s[tag=levelCreator,score_level_min=1,score_level=1] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level1"}

# Activating the structure block
execute @s[tag=levelCreator] ~ ~ ~ setblock ~ ~-2 ~ minecraft:redstone_block

# LAG!!!

# Giving the players jump_boost to prevent fall damage
effect @s[type=Player] minecraft:jump_boost 1 150 true

# Enabling the reload trigger for players
scoreboard players enable @s reload

scoreboard players set @s reload 2