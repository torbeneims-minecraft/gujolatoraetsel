#2017-07-07 12:18 - Torben

scoreboard players reset @e[tag=reload] reload
spawnpoint @a[tag=reload]
scoreboard players tag @e[tag=reload] remove reload

# Transferring the reload score of the players to the one of the levelCreator
execute @e[score_reload_min=1,score_reload=1,team=aqua] ~ ~ ~ scoreboard players set @e[team=aqua] reload 1
execute @e[score_reload_min=1,score_reload=1,team=black] ~ ~ ~ scoreboard players set @e[team=black] reload 1
execute @e[score_reload_min=1,score_reload=1,team=blue] ~ ~ ~ scoreboard players set @e[team=blue] reload 1
execute @e[score_reload_min=1,score_reload=1,team=cyan] ~ ~ ~ scoreboard players set @e[team=cyan] reload 1
execute @e[score_reload_min=1,score_reload=1,team=dark_blue] ~ ~ ~ scoreboard players set @e[team=dark_blue] reload 1
execute @e[score_reload_min=1,score_reload=1,team=gray] ~ ~ ~ scoreboard players set @e[team=gray] reload 1
execute @e[score_reload_min=1,score_reload=1,team=green] ~ ~ ~ scoreboard players set @e[team=green] reload 1
execute @e[score_reload_min=1,score_reload=1,team=purple] ~ ~ ~ scoreboard players set @e[team=purple] reload 1
execute @e[score_reload_min=1,score_reload=1,team=dark_red] ~ ~ ~ scoreboard players set @e[team=dark_red] reload 1
execute @e[score_reload_min=1,score_reload=1,team=gold] ~ ~ ~ scoreboard players set @e[team=gold] reload 1
execute @e[score_reload_min=1,score_reload=1,team=light_gray] ~ ~ ~ scoreboard players set @e[team=light_gray] reload 1
execute @e[score_reload_min=1,score_reload=1,team=lime] ~ ~ ~ scoreboard players set @e[team=lime] reload 1
execute @e[score_reload_min=1,score_reload=1,team=magenta] ~ ~ ~ scoreboard players set @e[team=magenta] reload 1
execute @e[score_reload_min=1,score_reload=1,team=red] ~ ~ ~ scoreboard players set @e[team=red] reload 1
execute @e[score_reload_min=1,score_reload=1,team=white] ~ ~ ~ scoreboard players set @e[team=white] reload 1
execute @e[score_reload_min=1,score_reload=1,team=yellow] ~ ~ ~ scoreboard players set @e[team=yellow] reload 1

# Setting the loaded_level score
execute @e[tag=levelCreator,score_reload_min=1,score_reload=1] ~ ~ ~ scoreboard players operation @s loaded_level = @s level

execute @e[score_reload_min=3,score_reload=3] ~ ~ ~ function gujolatoraetsel:reload/3
execute @e[score_reload_min=2,score_reload=2] ~ ~ ~ function gujolatoraetsel:reload/2
execute @e[score_reload_min=1,score_reload=1] ~ ~ ~ function gujolatoraetsel:reload/1

execute @e[c=1,score_reload_min=2,score_reload=2] ~ ~ ~ tellraw @a {"text":"Erwarte Lag"}