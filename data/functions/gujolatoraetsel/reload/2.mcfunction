# 2017-07-11 13:04 - Torben

# Giving the effects
effect @s[type=Player] clear
effect @s[type=Player] minecraft:instant_health 1 255 false
effect @s[type=Player] minecraft:levitation 3 2 true
effect @s[type=Player] minecraft:saturation 1 255 true
effect @s[type=Player] minecraft:regeneration 1 255 true
effect @s[type=Player] minecraft:resistance 1 7 true

# Removing the old level
execute @s[tag=levelCreator] ~ ~ ~ fill ~ ~ ~ ~31 ~31 ~31 air
# Currently moved after creation execute @s[tag=levelCreator] ~ ~ ~ fill ~ ~-2 ~ ~ ~-1 ~ air
execute @s[tag=levelCreator] ~ ~ ~ kill @e[dx=31,dy=18,dz=31,tag=!levelCreator,type=!Player]
execute @s[tag=levelCreator] ~ ~ ~ kill @e[dx=31,dy=18,dz=31,tag=!levelCreator,type=!Player]

clear @s[type=Player]
xp -100L @s[type=Player]

# Setting the structure block corresponding to the level 
execute @s[tag=levelCreator,score_level_min=-1,score_level=-1] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"LevelStart"}
execute @s[tag=levelCreator,score_level_min=1,score_level=1] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level1"}
execute @s[tag=levelCreator,score_level_min=2,score_level=2] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level2Alt"}
execute @s[tag=levelCreator,score_level_min=3,score_level=3] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level3"}
execute @s[tag=levelCreator,score_level_min=4,score_level=4] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level4Alt"}
execute @s[tag=levelCreator,score_level_min=5,score_level=5] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level5Alt"}
execute @s[tag=levelCreator,score_level_min=6,score_level=6] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level6Alt2"}
execute @s[tag=levelCreator,score_level_min=7,score_level=7] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level7"}
execute @s[tag=levelCreator,score_level_min=8,score_level=8] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level8"}
execute @s[tag=levelCreator,score_level_min=9,score_level=9] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level9"}
execute @s[tag=levelCreator,score_level_min=10,score_level=10] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level10"}
execute @s[tag=levelCreator,score_level_min=11,score_level=11] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level11"}
execute @s[tag=levelCreator,score_level_min=12,score_level=12] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level12"}
execute @s[tag=levelCreator,score_level_min=13,score_level=13] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level13"}
execute @s[tag=levelCreator,score_level_min=14,score_level=14] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level14"}
execute @s[tag=levelCreator,score_level_min=15,score_level=15] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level15"}
execute @s[tag=levelCreator,score_level_min=16,score_level=16] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level16"}
execute @s[tag=levelCreator,score_level_min=17,score_level=17] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level17"}
execute @s[tag=levelCreator,score_level_min=18,score_level=18] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level18"}
execute @s[tag=levelCreator,score_level_min=19,score_level=19] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level19"}
execute @s[tag=levelCreator,score_level_min=20,score_level=20] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level20"}
execute @s[tag=levelCreator,score_level_min=21,score_level=21] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level21"}
execute @s[tag=levelCreator,score_level_min=22,score_level=22] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level22"}
execute @s[tag=levelCreator,score_level_min=23,score_level=23] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level23"}
execute @s[tag=levelCreator,score_level_min=24,score_level=24] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level24"}
execute @s[tag=levelCreator,score_level_min=25,score_level=25] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level25"}
execute @s[tag=levelCreator,score_level_min=26,score_level=26] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level26"}
execute @s[tag=levelCreator,score_level_min=27,score_level=27] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level27"}
execute @s[tag=levelCreator,score_level_min=28,score_level=28] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level28"}
execute @s[tag=levelCreator,score_level_min=29,score_level=29] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level29"}
execute @s[tag=levelCreator,score_level_min=30,score_level=30] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level30"}
execute @s[tag=levelCreator,score_level_min=31,score_level=31] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level31"}
execute @s[tag=levelCreator,score_level_min=32,score_level=32] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level32"}


# Activating the structure block
execute @s[tag=levelCreator] ~ ~ ~ setblock ~ ~-2 ~ minecraft:redstone_block

# LAG!!!

execute @s[tag=levelCreator] ~ ~ ~ fill ~ ~-2 ~ ~ ~-1 ~ air

scoreboard players set @s reload 3