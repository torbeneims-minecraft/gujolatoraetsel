effect @s[type=Player] clear

clear @s[type=Player]
clear @s[type=Player]
# Teleporting the players (Level creators may choose to tp players to a specific spot in their level afterwards)
execute @e[tag=levelCreator,team=aqua] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=aqua,score_reload_min=3,score_reload=3]
execute @e[tag=levelCreator,team=black] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=black,score_reload_min=3,score_reload=3]
execute @e[tag=levelCreator,team=blue] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=blue,score_reload_min=3,score_reload=3]
execute @e[tag=levelCreator,team=cyan] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=cyan,score_reload_min=3,score_reload=3]
execute @e[tag=levelCreator,team=dark_blue] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=dark_blue,score_reload_min=3,score_reload=3]
execute @e[tag=levelCreator,team=gray] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=gray,score_reload_min=3,score_reload=3]
execute @e[tag=levelCreator,team=green] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=green,score_reload_min=3,score_reload=3]
execute @e[tag=levelCreator,team=purple] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=purple,score_reload_min=3,score_reload=3]
execute @e[tag=levelCreator,team=dark_red] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=dark_red,score_reload_min=3,score_reload=3]
execute @e[tag=levelCreator,team=gold] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=gold,score_reload_min=3,score_reload=3]
execute @e[tag=levelCreator,team=light_gray] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=light_gray,score_reload_min=3,score_reload=3]
execute @e[tag=levelCreator,team=lime] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=lime,score_reload_min=3,score_reload=3]
execute @e[tag=levelCreator,team=magenta] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=magenta,score_reload_min=3,score_reload=3]
execute @e[tag=levelCreator,team=red] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=red,score_reload_min=3,score_reload=3]
execute @e[tag=levelCreator,team=white] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=white,score_reload_min=3,score_reload=3]
execute @e[tag=levelCreator,team=yellow] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=yellow,score_reload_min=3,score_reload=3]

# Moved to reload (Level specific spawnpoint)
#spawnpoint @s[type=Player]

title @s[type=Player] reset
title @s[type=Player] times 8 28 9

scoreboard players tag @s add reload
# Moved to reload
#scoreboard players add @s reload 4
