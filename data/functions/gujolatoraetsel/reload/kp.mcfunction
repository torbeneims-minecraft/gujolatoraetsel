2017-07-10 22:13 - Torben

# Giving the effects
effect @s[type=Player] clear
effect @s[type=Player] minecraft:instant_health 1 255 false
# effect @s[type=Player] minecraft:blindness 3 255 false
effect @s[type=Player] minecraft:saturation 1 255 true
effect @s[type=Player] minecraft:regeneration 1 255 true
effect @s[type=Player] minecraft:resistance 1 7 true

scoreboard players set @s reload 2
