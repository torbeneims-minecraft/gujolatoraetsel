#2017-07-07 12:18 - Torben

function gujolatoraetsel:success if @e[score_success_min=1,score_success=2]

function gujolatoraetsel:reload if @e[score_reload_min=1]

# Resetting the level score for every player without (playing) team
scoreboard players reset @a[team=!aqua,team=!black,team=!blue,team=!cyan,team=!dark_blue,team=!gray,team=!green,team=!purple,team=!dark_red,team=!gold,team=!light_gray,team=!lime,team=!magenta,team=!red,team=!white,team=!yellow] level

# Transferring the level score back
scoreboard players operation @a[team=aqua] level = @e[tag=levelCreator,team=aqua] level
scoreboard players operation @a[team=black] level = @e[tag=levelCreator,team=black] level
scoreboard players operation @a[team=blue] level = @e[tag=levelCreator,team=blue] level
scoreboard players operation @a[team=cyan] level = @e[tag=levelCreator,team=cyan] level
scoreboard players operation @a[team=dark_blue] level = @e[tag=levelCreator,team=dark_blue] level
scoreboard players operation @a[team=gray] level = @e[tag=levelCreator,team=gray] level
scoreboard players operation @a[team=green] level = @e[tag=levelCreator,team=green] level
scoreboard players operation @a[team=purple] level = @e[tag=levelCreator,team=purple] level
scoreboard players operation @a[team=dark_red] level = @e[tag=levelCreator,team=dark_red] level
scoreboard players operation @a[team=gold] level = @e[tag=levelCreator,team=gold] level
scoreboard players operation @a[team=light_gray] level = @e[tag=levelCreator,team=light_gray] level
scoreboard players operation @a[team=lime] level = @e[tag=levelCreator,team=lime] level
scoreboard players operation @a[team=magenta] level = @e[tag=levelCreator,team=magenta] level
scoreboard players operation @a[team=red] level = @e[tag=levelCreator,team=red] level
scoreboard players operation @a[team=white] level = @e[tag=levelCreator,team=white] level
scoreboard players operation @a[team=yellow] level = @e[tag=levelCreator,team=yellow] level

function gujolatoraetsel:display

# Enabling the triggers
scoreboard players enable @a[team=!builder] reload
scoreboard players enable @a builder
scoreboard players enable @a info

function gujolatoraetsel:checkpoint if @e[tag=Checkpoint]
function gujolatoraetsel:healthpoint if @e[tag=Healthpoint]
function gujolatoraetsel:forceField if @e[tag=forceField]
function gujolatoraetsel:level_selection

kill @e[type=wither]