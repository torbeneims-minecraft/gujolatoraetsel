#2017-07-07 12:18 - Torben

# Show success
execute @a[score_success_min=1,score_success=1] ~ ~ ~ tellraw @a [{"selector":"@s"},{"text":" hat Level "},{"score":{"name":"@s","objective":"level"}},{"text":" geschafft!"}]
execute @a[score_success_min=1,score_success=1] ~ ~ ~ summon minecraft:fireworks_rocket ~ ~ ~

#Show skip
execute @a[score_success_min=2,score_success=2] ~ ~ ~ tellraw @a [{"selector":"@s"},{"text":" hat Level "},{"score":{"name":"@s","objective":"level"}},{"text":" übersprungen!"}]
scoreboard players set @a[score_success_min=2,score_success=2] success 1

# Transferring the success score of the players to the one of the levelCreator
execute @a[score_success_min=1,score_success=1,team=aqua] ~ ~ ~ scoreboard players set @e[team=aqua,tag=levelCreator] success 1
execute @a[score_success_min=1,score_success=1,team=black] ~ ~ ~ scoreboard players set @e[team=black,tag=levelCreator] success 1
execute @a[score_success_min=1,score_success=1,team=blue] ~ ~ ~ scoreboard players set @e[team=blue,tag=levelCreator] success 1
execute @a[score_success_min=1,score_success=1,team=cyan] ~ ~ ~ scoreboard players set @e[team=cyan,tag=levelCreator] success 1
execute @a[score_success_min=1,score_success=1,team=dark_blue] ~ ~ ~ scoreboard players set @e[team=dark_blue,tag=levelCreator] success 1
execute @a[score_success_min=1,score_success=1,team=gray] ~ ~ ~ scoreboard players set @e[team=gray,tag=levelCreator] success 1
execute @a[score_success_min=1,score_success=1,team=green] ~ ~ ~ scoreboard players set @e[team=green,tag=levelCreator] success 1
execute @a[score_success_min=1,score_success=1,team=purple] ~ ~ ~ scoreboard players set @e[team=purple,tag=levelCreator] success 1
execute @a[score_success_min=1,score_success=1,team=dark_red] ~ ~ ~ scoreboard players set @e[team=dark_red,tag=levelCreator] success 1
execute @a[score_success_min=1,score_success=1,team=gold] ~ ~ ~ scoreboard players set @e[team=gold,tag=levelCreator] success 1
execute @a[score_success_min=1,score_success=1,team=light_gray] ~ ~ ~ scoreboard players set @e[team=light_gray,tag=levelCreator] success 1
execute @a[score_success_min=1,score_success=1,team=lime] ~ ~ ~ scoreboard players set @e[team=lime,tag=levelCreator] success 1
execute @a[score_success_min=1,score_success=1,team=magenta] ~ ~ ~ scoreboard players set @e[team=magenta,tag=levelCreator] success 1
execute @a[score_success_min=1,score_success=1,team=red] ~ ~ ~ scoreboard players set @e[team=red,tag=levelCreator] success 1
execute @a[score_success_min=1,score_success=1,team=white] ~ ~ ~ scoreboard players set @e[team=white,tag=levelCreator] success 1
execute @a[score_success_min=1,score_success=1,team=yellow] ~ ~ ~ scoreboard players set @e[team=yellow,tag=levelCreator] success 1

#Increasing the level score by one
scoreboard players add @e[tag=levelCreator,score_success_min=1,score_success=1] level 1

scoreboard players set @e[score_success_min=1,score_success=1] reload 1

scoreboard players reset @e[score_success_min=1,score_success=1] success