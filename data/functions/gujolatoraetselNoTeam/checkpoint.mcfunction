# particle mobSpell x y z R G B Speed Cnt=0 mode player
execute @e[type=armor_stand,tag=Checkpoint] ~ ~ ~ particle mobSpell ~ ~ ~ 0 1 0.85 0.8 0 normal 

execute @e[type=armor_stand,tag=Checkpoint] ~ ~ ~ scoreboard player tag add @a[r=1] eligibleCheckpoint
scoreboard players tag @a[tag=eligibleCheckpoint] remove eligibleCheckpoint {OnGround:0b}
execute @a[tag=eligibleCheckpoint] ~ ~ ~ detect ~ ~-1 ~ air * scoreboard players tag remove @s eligibleCheckpoint
execute @e[type=armor_stand,tag=Checkpoint] ~ ~ ~ execute @a[r=1,tag=eligibleCheckpoint] ~ ~ ~ spawnpoint @s