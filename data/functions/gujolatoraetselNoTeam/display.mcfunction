#2017-07-07 12:07 - Torben

#execute @a[team=aqua,score_level_min=0] ~ ~ ~ title @s actionbar [{"text":"► Team Aqua ● Level ","color":"aqua"},{"score":{"name":"@s","objective":"level"},"color":"aqua"},{"text":" ◄","color":"aqua"}]
#execute @a[team=black,score_level_min=0] ~ ~ ~ title @s actionbar [{"text":"► Team Schwarz ● Level ","color":"black"},{"score":{"name":"@s","objective":"level"},"color":"black"},{"text":" ◄","color":"black"}]
#execute @a[team=blue,score_level_min=0] ~ ~ ~ title @s actionbar [{"text":"► Team Blau ● Level ","color":"blue"},{"score":{"name":"@s","objective":"level"},"color":"blue"},{"text":" ◄","color":"blue"}]
#execute @a[team=cyan,score_level_min=0] ~ ~ ~ title @s actionbar [{"text":"► Team Cyan ● Level ","color":"dark_aqua"},{"score":{"name":"@s","objective":"level"},"color":"dark_aqua"},{"text":" ◄","color":"dark_aqua"}]
#execute @a[team=dark_blue,score_level_min=0] ~ ~ ~ title @s actionbar [{"text":"► Team Dunkelblau ● Level ","color":"dark_blue"},{"score":{"name":"@s","objective":"level"},"color":"dark_blue"},{"text":" ◄","color":"dark_blue"}]
#execute @a[team=gray,score_level_min=0] ~ ~ ~ title @s actionbar [{"text":"► Team Grau ● Level ","color":"dark_gray"},{"score":{"name":"@s","objective":"level"},"color":"dark_gray"},{"text":" ◄","color":"dark_gray"}]
#execute @a[team=green,score_level_min=0] ~ ~ ~ title @s actionbar [{"text":"► Team Grün ● Level ","color":"dark_green"},{"score":{"name":"@s","objective":"level"},"color":"dark_green"},{"text":" ◄","color":"dark_green"}]
#execute @a[team=purple,score_level_min=0] ~ ~ ~ title @s actionbar [{"text":"► Team Lila ● Level ","color":"dark_purple"},{"score":{"name":"@s","objective":"level"},"color":"dark_purple"},{"text":" ◄","color":"dark_purple"}]
#execute @a[team=dark_red,score_level_min=0] ~ ~ ~ title @s actionbar [{"text":"► Team Dunkelrot ● Level ","color":"dark_red"},{"score":{"name":"@s","objective":"level"},"color":"dark_red"},{"text":" ◄","color":"dark_red"}]
#execute @a[team=gold,score_level_min=0] ~ ~ ~ title @s actionbar [{"text":"► Team Gold ● Level ","color":"gold"},{"score":{"name":"@s","objective":"level"},"color":"gold"},{"text":" ◄","color":"gold"}]
#execute @a[team=light_gray,score_level_min=0] ~ ~ ~ title @s actionbar [{"text":"► Team Hellgrau ● Level ","color":"gray"},{"score":{"name":"@s","objective":"level"},"color":"gray"},{"text":" ◄","color":"gray"}]
#execute @a[team=lime,score_level_min=0] ~ ~ ~ title @s actionbar [{"text":"► Team Lime ● Level ","color":"green"},{"score":{"name":"@s","objective":"level"},"color":"green"},{"text":" ◄","color":"green"}]
#execute @a[team=magenta,score_level_min=0] ~ ~ ~ title @s actionbar [{"text":"► Team Magenta ● Level ","color":"light_purple"},{"score":{"name":"@s","objective":"level"},"color":"light_purple"},{"text":" ◄","color":"light_purple"}]
#execute @a[team=red,score_level_min=0] ~ ~ ~ title @s actionbar [{"text":"► Team Rot ● Level ","color":"red"},{"score":{"name":"@s","objective":"level"},"color":"red"},{"text":" ◄","color":"red"}]
#execute @a[team=white,score_level_min=0] ~ ~ ~ title @s actionbar [{"text":"► Team Weiß ● Level ","color":"white"},{"score":{"name":"@s","objective":"level"},"color":"white"},{"text":" ◄","color":"white"}]
#execute @a[team=yellow,score_level_min=0] ~ ~ ~ title @s actionbar [{"text":"► Team Gelb ● Level ","color":"yellow"},{"score":{"name":"@s","objective":"level"},"color":"yellow"},{"text":" ◄","color":"yellow"}]

# Resetting the level and display_level score for every Builder
scoreboard players reset @a[team=builder,score_level_min=0] level
scoreboard players reset @a[team=builder,score_level_min=0] display_level
# Setting the display_level score of everyone else = level
execute @a[score_level] ~ ~ ~ scoreboard players operation @s display_level = @s level

# Setting the gamemode for Builders to creative
gamemode c @a[team=builder,m=s]
gamemode c @a[team=builder,m=a] 

# Title for Builders
title @a[team=builder] actionbar ["",{"text":"░▒▓ Bauteam ▓▒░","color":"white"}]