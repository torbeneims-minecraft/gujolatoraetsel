# 2017-06-27 17:40 - Torben
scoreboard objectives add success dummy Erfolg
scoreboard objectives add reload trigger Reload Status
scoreboard players enable @a reload
scoreboard objectives add level dummy
scoreboard objectives add display_level dummy Level

#scoreboard teams add white
#scoreboard teams add orange
#scoreboard teams add magenta
#scoreboard teams add light_blue
#scoreboard teams add yellow
#scoreboard teams add lime
#scoreboard teams add pink
#scoreboard teams add gray
#scoreboard teams add light_gray
#scoreboard teams add cyan
#scoreboard teams add purple
#scoreboard teams add blue
#scoreboard teams add brown
#scoreboard teams add green
#scoreboard teams add red
#scoreboard teams add black

#scoreboard teams option white color white
#scoreboard teams option orange color 
#scoreboard teams option magenta color 
#scoreboard teams option light_blue color 
#scoreboard teams option yellow color 
#scoreboard teams option lime color 
#scoreboard teams option pink color 
#scoreboard teams option gray color 
#scoreboard teams option light_gray color 
#scoreboard teams option cyan color 
#scoreboard teams option purple color 
#scoreboard teams option blue color 
#scoreboard teams option brown color 
#scoreboard teams option green color 
#scoreboard teams option red color 
#scoreboard teams option black color black

scoreboard teams add aqua Aqua
scoreboard teams add black Schwarz
scoreboard teams add blue Blau
scoreboard teams add cyan Cyan
scoreboard teams add dark_blue Dunkelblau
scoreboard teams add gray Grau
scoreboard teams add green Grün
scoreboard teams add purple Lila
scoreboard teams add dark_red Dunkelrot
scoreboard teams add gold Gold
scoreboard teams add light_gray Hellgrau
scoreboard teams add lime Lime
scoreboard teams add magenta Magenta
scoreboard teams add red Rot
scoreboard teams add white Weiß
scoreboard teams add yellow Gelb

scoreboard teams add builder Bauteam

scoreboard teams option aqua color aqua
scoreboard teams option black color black
scoreboard teams option blue color blue
scoreboard teams option cyan color dark_aqua
scoreboard teams option dark_blue color dark_blue
scoreboard teams option gray color dark_gray
scoreboard teams option green color dark_green
scoreboard teams option purple color dark_purple
scoreboard teams option dark_red color dark_red
scoreboard teams option gold color gold
scoreboard teams option light_gray color gray
scoreboard teams option lime color green
scoreboard teams option magenta color light_purple
scoreboard teams option red color red
scoreboard teams option white color white
scoreboard teams option yellow color yellow