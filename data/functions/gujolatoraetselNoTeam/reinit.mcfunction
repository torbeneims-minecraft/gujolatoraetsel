scoreboard objectives remove success
scoreboard objectives remove reload
scoreboard objectives remove level
scoreboard objectives remove display_level

scoreboard teams remove aqua
scoreboard teams remove black
scoreboard teams remove blue 
scoreboard teams remove cyan
scoreboard teams remove dark_blue
scoreboard teams remove gray
scoreboard teams remove green
scoreboard teams remove purple
scoreboard teams remove dark_red
scoreboard teams remove gold
scoreboard teams remove light_gray
scoreboard teams remove lime
scoreboard teams remove magenta
scoreboard teams remove red
scoreboard teams remove white
scoreboard teams remove yellow

scoreboard teams remove builder

function gujolatoraetsel:init