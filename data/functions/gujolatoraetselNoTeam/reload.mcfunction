
# Giving the effects
effect @s[type=Player] clear
effect @s[type=Player] minecraft:instant_health 1 255 false
effect @s[type=Player] minecraft:saturation 1 255 true
effect @s[type=Player] minecraft:regeneration 1 255 true
effect @s[type=Player] minecraft:resistance 1 7 true

# Removing the old level
execute @s[tag=levelCreator] ~ ~ ~ fill ~ ~ ~ ~31 ~31 ~31 air
execute @s[tag=levelCreator] ~ ~ ~ fill ~ ~-2 ~ ~ ~ ~ air
execute @s[tag=levelCreator] ~ ~ ~ kill @e[dx=31,dy=18,dz=31,tag=!levelCreator,type=!Player]
execute @s[tag=levelCreator] ~ ~ ~ kill @e[dx=31,dy=18,dz=31,tag=!levelCreator,type=!Player]

clear @s[type=Player]
xp @s[type=Player] -100L
# title @s[type=Player] title title {"text":"Loading Level ..."}
# execute @s[tag=levelCreator] ~ ~ ~ tellraw @a {"text":"Generating a level. Expect lag for a short while."}

# Setting the structure block corresponding to the level 
execute @s[tag=levelCreator,score_level_min=1,score_level=1] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level1"}
execute @s[tag=levelCreator,score_level_min=2,score_level=2] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level2"}
execute @s[tag=levelCreator,score_level_min=3,score_level=3] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level3"}
execute @s[tag=levelCreator,score_level_min=4,score_level=4] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level4"}
execute @s[tag=levelCreator,score_level_min=5,score_level=5] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level5"}
execute @s[tag=levelCreator,score_level_min=6,score_level=6] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level6"}
execute @s[tag=levelCreator,score_level_min=7,score_level=7] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level7"}
execute @s[tag=levelCreator,score_level_min=8,score_level=8] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level8"}
execute @s[tag=levelCreator,score_level_min=9,score_level=9] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level9"}
execute @s[tag=levelCreator,score_level_min=10,score_level=10] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level10"}
execute @s[tag=levelCreator,score_level_min=11,score_level=11] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level11"}
execute @s[tag=levelCreator,score_level_min=12,score_level=12] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level12"}
execute @s[tag=levelCreator,score_level_min=13,score_level=13] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level13"}
execute @s[tag=levelCreator,score_level_min=14,score_level=14] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level14"}
execute @s[tag=levelCreator,score_level_min=15,score_level=15] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level15"}
execute @s[tag=levelCreator,score_level_min=16,score_level=16] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level16"}
execute @s[tag=levelCreator,score_level_min=17,score_level=17] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level17"}
execute @s[tag=levelCreator,score_level_min=18,score_level=18] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level18"}
execute @s[tag=levelCreator,score_level_min=19,score_level=19] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level19"}
execute @s[tag=levelCreator,score_level_min=20,score_level=20] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level20"}
execute @s[tag=levelCreator,score_level_min=21,score_level=21] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level21"}
execute @s[tag=levelCreator,score_level_min=22,score_level=22] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level22"}
execute @s[tag=levelCreator,score_level_min=23,score_level=23] ~ ~ ~ setblock ~ ~-1 ~ minecraft:structure_block 0 replace {ignoreEntities:0b,mode:"LOAD",posY:1,sizeX:32,sizeY:18,sizeZ:32,name:"Level23"}


# Activating the structure block
execute @s[tag=levelCreator] ~ ~ ~ setblock ~ ~-2 ~ minecraft:redstone_block

# LAG!!!

#effect @s[type=Player] clear

# Giving the players jump_boost to prevent fall damage
#effect @s[type=Player] minecraft:jump_boost 1 150 true

# Teleporting the players (Level creators may choose to tp players to a specific spot in their level afterwards)
execute @e[tag=levelCreator,team=aqua] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=aqua,score_reload_min=1,score_reload=2]
execute @e[tag=levelCreator,team=black] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=black,score_reload_min=1,score_reload=2]
execute @e[tag=levelCreator,team=blue] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=blue,score_reload_min=1,score_reload=2]
execute @e[tag=levelCreator,team=cyan] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=cyan,score_reload_min=1,score_reload=2]
execute @e[tag=levelCreator,team=dark_blue] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=dark_blue,score_reload_min=1,score_reload=2]
execute @e[tag=levelCreator,team=gray] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=gray,score_reload_min=1,score_reload=2]
execute @e[tag=levelCreator,team=green] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=green,score_reload_min=1,score_reload=2]
execute @e[tag=levelCreator,team=purple] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=purple,score_reload_min=1,score_reload=2]
execute @e[tag=levelCreator,team=dark_red] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=dark_red,score_reload_min=1,score_reload=2]
execute @e[tag=levelCreator,team=gold] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=gold,score_reload_min=1,score_reload=2]
execute @e[tag=levelCreator,team=light_gray] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=light_gray,score_reload_min=1,score_reload=2]
execute @e[tag=levelCreator,team=lime] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=lime,score_reload_min=1,score_reload=2]
execute @e[tag=levelCreator,team=magenta] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=magenta,score_reload_min=1,score_reload=2]
execute @e[tag=levelCreator,team=red] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=red,score_reload_min=1,score_reload=2]
execute @e[tag=levelCreator,team=white] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=white,score_reload_min=1,score_reload=2]
execute @e[tag=levelCreator,team=yellow] ~ ~ ~ spreadplayers ~1 ~1 1 2 false @a[team=yellow,score_reload_min=1,score_reload=2]

spawnpoint @s[type=Player]

scoreboard players set @s reload 3