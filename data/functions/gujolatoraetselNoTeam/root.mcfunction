#2017-07-07 12:07 - Torben

scoreboard players reset @e[score_reload_min=3] reload

# Transferring the success score of the players to the one of the levelCreator
execute @a[score_success_min=1,score_success=1,team=aqua] ~ ~ ~ scoreboard players set @e[team=aqua] success 1
execute @a[score_success_min=1,score_success=1,team=black] ~ ~ ~ scoreboard players set @e[team=black] success 1
execute @a[score_success_min=1,score_success=1,team=blue] ~ ~ ~ scoreboard players set @e[team=blue] success 1
execute @a[score_success_min=1,score_success=1,team=cyan] ~ ~ ~ scoreboard players set @e[team=cyan] success 1
execute @a[score_success_min=1,score_success=1,team=dark_blue] ~ ~ ~ scoreboard players set @e[team=dark_blue] success 1
execute @a[score_success_min=1,score_success=1,team=gray] ~ ~ ~ scoreboard players set @e[team=gray] success 1
execute @a[score_success_min=1,score_success=1,team=green] ~ ~ ~ scoreboard players set @e[team=green] success 1
execute @a[score_success_min=1,score_success=1,team=purple] ~ ~ ~ scoreboard players set @e[team=purple] success 1
execute @a[score_success_min=1,score_success=1,team=dark_red] ~ ~ ~ scoreboard players set @e[team=dark_red] success 1
execute @a[score_success_min=1,score_success=1,team=gold] ~ ~ ~ scoreboard players set @e[team=gold] success 1
execute @a[score_success_min=1,score_success=1,team=light_gray] ~ ~ ~ scoreboard players set @e[team=light_gray] success 1
execute @a[score_success_min=1,score_success=1,team=lime] ~ ~ ~ scoreboard players set @e[team=lime] success 1
execute @a[score_success_min=1,score_success=1,team=magenta] ~ ~ ~ scoreboard players set @e[team=magenta] success 1
execute @a[score_success_min=1,score_success=1,team=red] ~ ~ ~ scoreboard players set @e[team=red] success 1
execute @a[score_success_min=1,score_success=1,team=white] ~ ~ ~ scoreboard players set @e[team=white] success 1
execute @a[score_success_min=1,score_success=1,team=yellow] ~ ~ ~ scoreboard players set @e[team=yellow] success 1

execute @e[score_success_min=1,score_success=1] ~ ~ ~ function gujolatoraetsel:success

# Transferring the reload score of the players to the one of the levelCreator
execute @a[score_reload_min=1,score_reload=1,team=aqua] ~ ~ ~ scoreboard players set @e[team=aqua] reload 1
execute @a[score_reload_min=1,score_reload=1,team=black] ~ ~ ~ scoreboard players set @e[team=black] reload 1
execute @a[score_reload_min=1,score_reload=1,team=blue] ~ ~ ~ scoreboard players set @e[team=blue] reload 1
execute @a[score_reload_min=1,score_reload=1,team=cyan] ~ ~ ~ scoreboard players set @e[team=cyan] reload 1
execute @a[score_reload_min=1,score_reload=1,team=dark_blue] ~ ~ ~ scoreboard players set @e[team=dark_blue] reload 1
execute @a[score_reload_min=1,score_reload=1,team=gray] ~ ~ ~ scoreboard players set @e[team=gray] reload 1
execute @a[score_reload_min=1,score_reload=1,team=green] ~ ~ ~ scoreboard players set @e[team=green] reload 1
execute @a[score_reload_min=1,score_reload=1,team=purple] ~ ~ ~ scoreboard players set @e[team=purple] reload 1
execute @a[score_reload_min=1,score_reload=1,team=dark_red] ~ ~ ~ scoreboard players set @e[team=dark_red] reload 1
execute @a[score_reload_min=1,score_reload=1,team=gold] ~ ~ ~ scoreboard players set @e[team=gold] reload 1
execute @a[score_reload_min=1,score_reload=1,team=light_gray] ~ ~ ~ scoreboard players set @e[team=light_gray] reload 1
execute @a[score_reload_min=1,score_reload=1,team=lime] ~ ~ ~ scoreboard players set @e[team=lime] reload 1
execute @a[score_reload_min=1,score_reload=1,team=magenta] ~ ~ ~ scoreboard players set @e[team=magenta] reload 1
execute @a[score_reload_min=1,score_reload=1,team=red] ~ ~ ~ scoreboard players set @e[team=red] reload 1
execute @a[score_reload_min=1,score_reload=1,team=white] ~ ~ ~ scoreboard players set @e[team=white] reload 1
execute @a[score_reload_min=1,score_reload=1,team=yellow] ~ ~ ~ scoreboard players set @e[team=yellow] reload 1

execute @e[score_reload_min=1,score_reload=2] ~ ~ ~ function gujolatoraetsel:reload

scoreboard players operation @a[team=aqua] level = @e[tag=levelCreator,team=aqua] level
scoreboard players operation @a[team=black] level = @e[tag=levelCreator,team=black] level
scoreboard players operation @a[team=blue] level = @e[tag=levelCreator,team=blue] level
scoreboard players operation @a[team=cyan] level = @e[tag=levelCreator,team=cyan] level
scoreboard players operation @a[team=dark_blue] level = @e[tag=levelCreator,team=dark_blue] level
scoreboard players operation @a[team=gray] level = @e[tag=levelCreator,team=gray] level
scoreboard players operation @a[team=green] level = @e[tag=levelCreator,team=green] level
scoreboard players operation @a[team=purple] level = @e[tag=levelCreator,team=purple] level
scoreboard players operation @a[team=dark_red] level = @e[tag=levelCreator,team=dark_red] level
scoreboard players operation @a[team=gold] level = @e[tag=levelCreator,team=gold] level
scoreboard players operation @a[team=light_gray] level = @e[tag=levelCreator,team=light_gray] level
scoreboard players operation @a[team=lime] level = @e[tag=levelCreator,team=lime] level
scoreboard players operation @a[team=magenta] level = @e[tag=levelCreator,team=magenta] level
scoreboard players operation @a[team=red] level = @e[tag=levelCreator,team=red] level
scoreboard players operation @a[team=white] level = @e[tag=levelCreator,team=white] level
scoreboard players operation @a[team=yellow] level = @e[tag=levelCreator,team=yellow] level

function gujolatoraetsel:display

# Enabling the reload trigger for players
scoreboard players enable @a reload


function gujolatoraetsel:checkpoint
function gujolatoraetsel:healthpoint