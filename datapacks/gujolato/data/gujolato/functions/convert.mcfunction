# converting unsupported tags
tag @e[tag=Checkpoint] add checkpoint
tag @e[tag=Checkpoint] remove Checkpoint
tag @e[tag=Healthpoint] add healthpoint
tag @e[tag=Healthpoint] remove Healthpoint
tag @e[tag=forceFiled] add forcefield
tag @e[tag=forceField] remove forceField