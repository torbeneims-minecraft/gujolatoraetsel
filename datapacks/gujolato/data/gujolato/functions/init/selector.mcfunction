execute positioned 900 61 0 run summon armor_stand ~-15 ~4 ~-15 {CustomNameVisible:1b,Team:"aqua",NoGravity:1b,Glowing:0b,Marker:1b,Invisible:1b,Tags:["level_selector"],CustomName:"{\"translate\":\"gujolato.teams.aqua.name\"}"}
execute positioned 900 61 0 run summon armor_stand ~-15 ~4 ~-05 {CustomNameVisible:1b,Team:"black",NoGravity:1b,Glowing:0b,Marker:1b,Invisible:1b,Tags:["level_selector"],CustomName:"{\"translate\":\"gujolato.teams.black.name\"}"}
execute positioned 900 61 0 run summon armor_stand ~-15 ~4 ~005 {CustomNameVisible:1b,Team:"blue",NoGravity:1b,Glowing:0b,Marker:1b,Invisible:1b,Tags:["level_selector"],CustomName:"{\"translate\":\"gujolato.teams.blue.name\"}"}
execute positioned 900 61 0 run summon armor_stand ~-15 ~4 ~015 {CustomNameVisible:1b,Team:"cyan",NoGravity:1b,Glowing:0b,Marker:1b,Invisible:1b,Tags:["level_selector"],CustomName:"{\"translate\":\"gujolato.teams.cyan.name\"}"}
execute positioned 900 61 0 run summon armor_stand ~-05 ~4 ~-15 {CustomNameVisible:1b,Team:"dark_blue",NoGravity:1b,Glowing:0b,Marker:1b,Invisible:1b,Tags:["level_selector"],CustomName:"{\"translate\":\"gujolato.teams.dark_blue.name\"}"}
execute positioned 900 61 0 run summon armor_stand ~-05 ~4 ~-05 {CustomNameVisible:1b,Team:"gray",NoGravity:1b,Glowing:0b,Marker:1b,Invisible:1b,Tags:["level_selector"],CustomName:"{\"translate\":\"gujolato.teams.gray.name\"}"}
execute positioned 900 61 0 run summon armor_stand ~-05 ~4 ~005 {CustomNameVisible:1b,Team:"green",NoGravity:1b,Glowing:0b,Marker:1b,Invisible:1b,Tags:["level_selector"],CustomName:"{\"translate\":\"gujolato.teams.green.name\"}"}
execute positioned 900 61 0 run summon armor_stand ~-05 ~4 ~015 {CustomNameVisible:1b,Team:"purple",NoGravity:1b,Glowing:0b,Marker:1b,Invisible:1b,Tags:["level_selector"],CustomName:"{\"translate\":\"gujolato.teams.purple.name\"}"}
execute positioned 900 61 0 run summon armor_stand ~005 ~4 ~-15 {CustomNameVisible:1b,Team:"dark_red",NoGravity:1b,Glowing:0b,Marker:1b,Invisible:1b,Tags:["level_selector"],CustomName:"{\"translate\":\"gujolato.teams.dark_red.name\"}"}
execute positioned 900 61 0 run summon armor_stand ~005 ~4 ~-05 {CustomNameVisible:1b,Team:"gold",NoGravity:1b,Glowing:0b,Marker:1b,Invisible:1b,Tags:["level_selector"],CustomName:"{\"translate\":\"gujolato.teams.gold.name\"}"}
execute positioned 900 61 0 run summon armor_stand ~005 ~4 ~005 {CustomNameVisible:1b,Team:"light_gray",NoGravity:1b,Glowing:0b,Marker:1b,Invisible:1b,Tags:["level_selector"],CustomName:"{\"translate\":\"gujolato.teams.light_gray.name\"}"}
execute positioned 900 61 0 run summon armor_stand ~005 ~4 ~015 {CustomNameVisible:1b,Team:"lime",NoGravity:1b,Glowing:0b,Marker:1b,Invisible:1b,Tags:["level_selector"],CustomName:"{\"translate\":\"gujolato.teams.lime.name\"}"}
execute positioned 900 61 0 run summon armor_stand ~015 ~4 ~-15 {CustomNameVisible:1b,Team:"magenta",NoGravity:1b,Glowing:0b,Marker:1b,Invisible:1b,Tags:["level_selector"],CustomName:"{\"translate\":\"gujolato.teams.magenta.name\"}"}
execute positioned 900 61 0 run summon armor_stand ~015 ~4 ~-05 {CustomNameVisible:1b,Team:"red",NoGravity:1b,Glowing:0b,Marker:1b,Invisible:1b,Tags:["level_selector"],CustomName:"{\"translate\":\"gujolato.teams.red.name\"}"}
execute positioned 900 61 0 run summon armor_stand ~015 ~4 ~005 {CustomNameVisible:1b,Team:"white",NoGravity:1b,Glowing:0b,Marker:1b,Invisible:1b,Tags:["level_selector"],CustomName:"{\"translate\":\"gujolato.teams.white.name\"}"}
execute positioned 900 61 0 run summon armor_stand ~015 ~4 ~015 {CustomNameVisible:1b,Team:"yellow",NoGravity:1b,Glowing:0b,Marker:1b,Invisible:1b,Tags:["level_selector"],CustomName:"{\"translate\":\"gujolato.teams.yellow.name\"}"}
execute positioned 900 61 0 run fill ~-21 ~1 ~-21 ~21 ~1 ~21 minecraft:barrier
execute positioned 900 61 0 run fill ~-22 ~3 ~-22 ~22 ~3 ~22 minecraft:barrier
execute positioned 900 61 0 run fill ~-21 ~3 ~-21 ~21 ~3 ~21 minecraft:air


execute at @e[tag=level_selector,team=aqua] run setblock ~-3 ~-4 ~-3 minecraft:structure_block{ignoreEntities:0b,mode:"LOAD",posY:0,sizeX:7,sizeY:32,sizeZ:7,name:"gujolato:sel/aqua"}
execute at @e[tag=level_selector,team=black] run setblock ~-3 ~-4 ~-3 minecraft:structure_block{ignoreEntities:0b,mode:"LOAD",posY:0,sizeX:7,sizeY:32,sizeZ:7,name:"gujolato:sel/black"}
execute at @e[tag=level_selector,team=blue] run setblock ~-3 ~-4 ~-3 minecraft:structure_block{ignoreEntities:0b,mode:"LOAD",posY:0,sizeX:7,sizeY:32,sizeZ:7,name:"gujolato:sel/blue"}
execute at @e[tag=level_selector,team=cyan] run setblock ~-3 ~-4 ~-3 minecraft:structure_block{ignoreEntities:0b,mode:"LOAD",posY:0,sizeX:7,sizeY:32,sizeZ:7,name:"gujolato:sel/cyan"}
execute at @e[tag=level_selector,team=dark_blue] run setblock ~-3 ~-4 ~-3 minecraft:structure_block{ignoreEntities:0b,mode:"LOAD",posY:0,sizeX:7,sizeY:32,sizeZ:7,name:"gujolato:sel/dark_blue"}
execute at @e[tag=level_selector,team=gray] run setblock ~-3 ~-4 ~-3 minecraft:structure_block{ignoreEntities:0b,mode:"LOAD",posY:0,sizeX:7,sizeY:32,sizeZ:7,name:"gujolato:sel/gray"}
execute at @e[tag=level_selector,team=green] run setblock ~-3 ~-4 ~-3 minecraft:structure_block{ignoreEntities:0b,mode:"LOAD",posY:0,sizeX:7,sizeY:32,sizeZ:7,name:"gujolato:sel/green"}
execute at @e[tag=level_selector,team=purple] run setblock ~-3 ~-4 ~-3 minecraft:structure_block{ignoreEntities:0b,mode:"LOAD",posY:0,sizeX:7,sizeY:32,sizeZ:7,name:"gujolato:sel/purple"}
execute at @e[tag=level_selector,team=dark_red] run setblock ~-3 ~-4 ~-3 minecraft:structure_block{ignoreEntities:0b,mode:"LOAD",posY:0,sizeX:7,sizeY:32,sizeZ:7,name:"gujolato:sel/dark_red"}
execute at @e[tag=level_selector,team=gold] run setblock ~-3 ~-4 ~-3 minecraft:structure_block{ignoreEntities:0b,mode:"LOAD",posY:0,sizeX:7,sizeY:32,sizeZ:7,name:"gujolato:sel/gold"}
execute at @e[tag=level_selector,team=light_gray] run setblock ~-3 ~-4 ~-3 minecraft:structure_block{ignoreEntities:0b,mode:"LOAD",posY:0,sizeX:7,sizeY:32,sizeZ:7,name:"gujolato:sel/light_gray"}
execute at @e[tag=level_selector,team=lime] run setblock ~-3 ~-4 ~-3 minecraft:structure_block{ignoreEntities:0b,mode:"LOAD",posY:0,sizeX:7,sizeY:32,sizeZ:7,name:"gujolato:sel/lime"}
execute at @e[tag=level_selector,team=magenta] run setblock ~-3 ~-4 ~-3 minecraft:structure_block{ignoreEntities:0b,mode:"LOAD",posY:0,sizeX:7,sizeY:32,sizeZ:7,name:"gujolato:sel/magenta"}
execute at @e[tag=level_selector,team=red] run setblock ~-3 ~-4 ~-3 minecraft:structure_block{ignoreEntities:0b,mode:"LOAD",posY:0,sizeX:7,sizeY:32,sizeZ:7,name:"gujolato:sel/red"}
execute at @e[tag=level_selector,team=white] run setblock ~-3 ~-4 ~-3 minecraft:structure_block{ignoreEntities:0b,mode:"LOAD",posY:0,sizeX:7,sizeY:32,sizeZ:7,name:"gujolato:sel/white"}
execute at @e[tag=level_selector,team=yellow] run setblock ~-3 ~-4 ~-3 minecraft:structure_block{ignoreEntities:0b,mode:"LOAD",posY:0,sizeX:7,sizeY:32,sizeZ:7,name:"gujolato:sel/yellow"}
execute at @e[tag=level_selector] run setblock ~-3 ~-3 ~-3 redstone_block