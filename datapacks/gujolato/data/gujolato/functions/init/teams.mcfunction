team add aqua {"translate":"gujolato.teams.aqua.name"}
team add black {"translate":"gujolato.teams.black.name"}
team add blue {"translate":"gujolato.teams.blue.name"}
team add cyan {"translate":"gujolato.teams.cyan.name"}
team add dark_blue {"translate":"gujolato.teams.dark_blue.name"}
team add gray {"translate":"gujolato.teams.gray.name"}
team add green {"translate":"gujolato.teams.green.name"}
team add purple {"translate":"gujolato.teams.purple.name"}
team add dark_red {"translate":"gujolato.teams.dark_red.name"}
team add gold {"translate":"gujolato.teams.gold.name"}
team add light_gray {"translate":"gujolato.teams.light_gray.name"}
team add lime {"translate":"gujolato.teams.lime.name"}
team add magenta {"translate":"gujolato.teams.magenta.name"}
team add red {"translate":"gujolato.teams.red.name"}
team add white {"translate":"gujolato.teams.white.name"}
team add yellow {"translate":"gujolato.teams.yellow.name"}

team modify aqua color aqua
team modify black color black
team modify blue color blue
team modify cyan color dark_aqua
team modify dark_blue color dark_blue
team modify gray color dark_gray
team modify green color dark_green
team modify purple color dark_purple
team modify dark_red color dark_red
team modify gold color gold
team modify light_gray color gray
team modify lime color green
team modify magenta color light_purple
team modify red color red
team modify white color white
team modify yellow color yellow

team modify aqua seeFriendlyInvisibles false
team modify black seeFriendlyInvisibles false
team modify blue seeFriendlyInvisibles false
team modify cyan seeFriendlyInvisibles false
team modify dark_blue seeFriendlyInvisibles false
team modify gray seeFriendlyInvisibles false
team modify green seeFriendlyInvisibles false
team modify purple seeFriendlyInvisibles false
team modify dark_red seeFriendlyInvisibles false
team modify gold seeFriendlyInvisibles false
team modify light_gray seeFriendlyInvisibles false
team modify lime seeFriendlyInvisibles false
team modify magenta seeFriendlyInvisibles false
team modify red seeFriendlyInvisibles false
team modify white seeFriendlyInvisibles false
team modify yellow seeFriendlyInvisibles false