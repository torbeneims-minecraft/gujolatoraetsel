scoreboard players add @e[scores={lvl25title=-100..}] lvl25title 1
scoreboard players reset @e[scores={lvl25title=700..}] lvl25title
effect give @a[scores={lvl25title=1}] blindness 36 0 true
title @a[scores={lvl25title=35}] title [{"translate":"gujolato.level.25.text.spawn_title.1","color":"green"}]
title @a[scores={lvl25title=70}] title [{"translate":"gujolato.level.25.text.spawn_title.2","color":"green"}]
title @a[scores={lvl25title=110}] title [{"translate":"gujolato.level.25.text.spawn_title.3","color":"green"}]
title @a[scores={lvl25title=145}] title [{"translate":"gujolato.level.25.text.spawn_title.4","color":"green"}]
title @a[scores={lvl25title=185}] title [{"translate":"gujolato.level.25.text.spawn_title.5","color":"green"}]
title @a[scores={lvl25title=215}] title [{"translate":"gujolato.level.25.text.spawn_title.6","color":"green"}]
title @a[scores={lvl25title=235}] title [{"translate":"gujolato.level.25.text.spawn_title.7","color":"green"}]
title @a[scores={lvl25title=260}] title [{"translate":"gujolato.level.25.text.spawn_title.8","color":"green"}]
title @a[scores={lvl25title=280}] title [{"translate":"gujolato.level.25.text.spawn_title.9","color":"green"}]
title @a[scores={lvl25title=315}] title [{"translate":"gujolato.level.25.text.spawn_title.10","color":"green"}]
title @a[scores={lvl25title=335}] title [{"translate":"gujolato.level.25.text.spawn_title.11","color":"green"}]
title @a[scores={lvl25title=392}] title [{"translate":"gujolato.level.25.text.spawn_title.12","color":"green"}]
execute at @a[limit=1,scores={lvl25title=402}] run playsound entity.firework_rocket.blast ambient @s ~ ~-1 ~ 1 0.1
title @a[scores={lvl25title=442}] title [{"translate":"gujolato.level.25.text.spawn_title.13","color":"green"}]
title @a[scores={lvl25title=467}] title [{"translate":"gujolato.level.25.text.spawn_title.14","color":"green"}]
title @a[scores={lvl25title=502}] title [{"translate":"gujolato.level.25.text.spawn_title.15","color":"green"}]
title @a[scores={lvl25title=537}] title [{"translate":"gujolato.level.25.text.spawn_title.16","color":"green"}]
title @a[scores={lvl25title=587}] title [{"translate":"gujolato.level.25.text.spawn_title.17","color":"green"}]
title @a[scores={lvl25title=622}] title [{"translate":"gujolato.level.25.text.spawn_title.18","color":"green"}]
title @a[scores={lvl25title=657}] title [{"translate":"gujolato.level.25.text.spawn_title.19","color":"green"}]
execute at @a[scores={lvl25title=637}] at @e[limit=1,sort=nearest,distance=..5,tag=level25-rakete] run setblock ~ ~ ~ stone_button[facing=south, face=wall]
#effect @a[score_level25-title_min=657,score_level25-title=657] blindness 1 255 true

#Crafting
execute at @a[scores={level=25},limit=1] at @e[limit=1,type=item,distance=..5,nbt={Item:{id:"minecraft:orange_concrete_powder",Count:3b}}] if entity @e[type=item,distance=..1,nbt={Item:{id:"minecraft:stick",Count:2b}}] run summon minecraft:item ~ ~ ~ {Item:{id:"minecraft:golden_pickaxe",Count:1b,tag:{CanDestroy:["minecraft:beacon","minecraft:black_terracotta"],display:{Name:"{\"translate\":\"gujolato.level.25.text.mars_pick.name\"}",Lore:["{\"translate\":\"gujolato.level.25.text.mars_pick.lore\"}"]},HideFlags:1,Enchantments:[{id:"minecraft:silk_touch",lvl:1s}]}}}
execute at @a[scores={level=25},limit=1] at @e[type=item,distance=..5,nbt={Item:{id:"minecraft:orange_concrete_powder",Count:3b}}] if entity @e[type=item,distance=..1,nbt={Item:{id:"minecraft:stick",Count:2b}}] run playsound minecraft:block.anvil.use player @a[distance=..5]
execute at @a[scores={level=25},limit=1] at @e[type=item,distance=..5,nbt={Item:{id:"minecraft:orange_concrete_powder",Count:3b}}] if entity @e[type=item,distance=..1,nbt={Item:{id:"minecraft:stick",Count:2b}}] run particle poof ~ ~ ~ 0 0 0 0.03 10 normal
execute at @a[scores={level=25},limit=1] at @e[type=item,distance=..5,nbt={Item:{id:"minecraft:orange_concrete_powder",Count:3b}}] if entity @e[type=item,distance=..1,nbt={Item:{id:"minecraft:stick",Count:2b}}] run kill @e[distance=..2,type=item,nbt=!{Item:{id:"minecraft:golden_pickaxe"}}]


#title @a[scores={lvl25title=35}] title [{"text":"Eines Tages,","color":"green"}]
#title @a[scores={lvl25title=70}] title [{"text":"als die Menschheit","color":"green"}]
#title @a[scores={lvl25title=110}] title [{"text":"einen bedeutenden Schritt","color":"green"}]
#title @a[scores={lvl25title=145}] title [{"text":"in die Zukunft wagte ","color":"green"}]
#title @a[scores={lvl25title=185}] title [{"text":"und den ersten","color":"green"}]
#title @a[scores={lvl25title=215}] title [{"text":"Menschen","color":"green"}]
#title @a[scores={lvl25title=235}] title [{"text":"auf den Mars","color":"green"}]
#title @a[scores={lvl25title=260}] title [{"text":"schickte,","color":"green"}]
#title @a[scores={lvl25title=280}] title [{"text":"ereignete sich","color":"green"}]
#title @a[scores={lvl25title=315}] title [{"text":"ein tragisches Unglück...","color":"green"}]
#title @a[scores={lvl25title=235}] title [{"text":"Ein Stein","color":"green"}]
#title @a[scores={lvl25title=392}] title [{"text":"schlug ein Loch","color":"green"}]
#execute at @a[limit=1,scores={lvl25title=402}] run playsound entity.firework.blast ambient @s ~ ~-1 ~ 1 0.1
#title @a[scores={lvl25title=442}] title [{"text":"in den Treibstofftank...","color":"green"}]
#title @a[scores={lvl25title=467}] title [{"text":"Doch fanden","color":"green"}]
#title @a[scores={lvl25title=502}] title [{"text":"die Wissenschaftler","color":"green"}]
#title @a[scores={lvl25title=537}] title [{"text":"zu DEINEM Glück","color":"green"}]
#title @a[scores={lvl25title=587}] title [{"text":"eine Treibstoffalternative...","color":"green"}]
#title @a[scores={lvl25title=622}] title [{"text":"Nun liegt es an dir","color":"green"}]
#title @a[scores={lvl25title=657}] title [{"text":"sie zu finden!","color":"green"}]