# Message
execute at @e[tag=level_selector,team=aqua] positioned ~-3 ~ ~-3 run tellraw @a[dx=6,dy=-2,dz=6,gamemode=!creative,team=!aqua] {"translate":"gujolato.text.join_team","with":[{"translate":"gujolato.teams.aqua.name","color":"aqua"}]}
execute at @e[tag=level_selector,team=black] positioned ~-3 ~ ~-3 run tellraw @a[dx=6,dy=-2,dz=6,gamemode=!creative,team=!black] {"translate":"gujolato.text.join_team","with":[{"translate":"gujolato.teams.black.name","color":"black"}]}
execute at @e[tag=level_selector,team=blue] positioned ~-3 ~ ~-3 run tellraw @a[dx=6,dy=-2,dz=6,gamemode=!creative,team=!blue] {"translate":"gujolato.text.join_team","with":[{"translate":"gujolato.teams.blue.name","color":"blue"}]}
execute at @e[tag=level_selector,team=cyan] positioned ~-3 ~ ~-3 run tellraw @a[dx=6,dy=-2,dz=6,gamemode=!creative,team=!cyan] {"translate":"gujolato.text.join_team","with":[{"translate":"gujolato.teams.cyan.name","color":"dark_aqua"}]}
execute at @e[tag=level_selector,team=dark_blue] positioned ~-3 ~ ~-3 run tellraw @a[dx=6,dy=-2,dz=6,gamemode=!creative,team=!dark_blue] {"translate":"gujolato.text.join_team","with":[{"translate":"gujolato.teams.dark_blue.name","color":"dark_blue"}]}
execute at @e[tag=level_selector,team=gray] positioned ~-3 ~ ~-3 run tellraw @a[dx=6,dy=-2,dz=6,gamemode=!creative,team=!gray] {"translate":"gujolato.text.join_team","with":[{"translate":"gujolato.teams.gray.name","color":"dark_gray"}]}
execute at @e[tag=level_selector,team=green] positioned ~-3 ~ ~-3 run tellraw @a[dx=6,dy=-2,dz=6,gamemode=!creative,team=!green] {"translate":"gujolato.text.join_team","with":[{"translate":"gujolato.teams.green.name","color":"dark_green"}]}
execute at @e[tag=level_selector,team=purple] positioned ~-3 ~ ~-3 run tellraw @a[dx=6,dy=-2,dz=6,gamemode=!creative,team=!purple] {"translate":"gujolato.text.join_team","with":[{"translate":"gujolato.teams.purple.name","color":"dark_purple"}]}
execute at @e[tag=level_selector,team=dark_red] positioned ~-3 ~ ~-3 run tellraw @a[dx=6,dy=-2,dz=6,gamemode=!creative,team=!dark_red] {"translate":"gujolato.text.join_team","with":[{"translate":"gujolato.teams.dark_red.name","color":"dark_red"}]}
execute at @e[tag=level_selector,team=gold] positioned ~-3 ~ ~-3 run tellraw @a[dx=6,dy=-2,dz=6,gamemode=!creative,team=!gold] {"translate":"gujolato.text.join_team","with":[{"translate":"gujolato.teams.gold.name","color":"gold"}]}
execute at @e[tag=level_selector,team=light_gray] positioned ~-3 ~ ~-3 run tellraw @a[dx=6,dy=-2,dz=6,gamemode=!creative,team=!light_gray] {"translate":"gujolato.text.join_team","with":[{"translate":"gujolato.teams.light_gray.name","color":"gray"}]}
execute at @e[tag=level_selector,team=lime] positioned ~-3 ~ ~-3 run tellraw @a[dx=6,dy=-2,dz=6,gamemode=!creative,team=!lime] {"translate":"gujolato.text.join_team","with":[{"translate":"gujolato.teams.lime.name","color":"green"}]}
execute at @e[tag=level_selector,team=magenta] positioned ~-3 ~ ~-3 run tellraw @a[dx=6,dy=-2,dz=6,gamemode=!creative,team=!magenta] {"translate":"gujolato.text.join_team","with":[{"translate":"gujolato.teams.magenta.name","color":"light_purple"}]}
execute at @e[tag=level_selector,team=red] positioned ~-3 ~ ~-3 run tellraw @a[dx=6,dy=-2,dz=6,gamemode=!creative,team=!red] {"translate":"gujolato.text.join_team","with":[{"translate":"gujolato.teams.red.name","color":"red"}]}
execute at @e[tag=level_selector,team=white] positioned ~-3 ~ ~-3 run tellraw @a[dx=6,dy=-2,dz=6,gamemode=!creative,team=!white] {"translate":"gujolato.text.join_team","with":[{"translate":"gujolato.teams.white.name","color":"white"}]}
execute at @e[tag=level_selector,team=yellow] positioned ~-3 ~ ~-3 run tellraw @a[dx=6,dy=-2,dz=6,gamemode=!creative,team=!yellow] {"translate":"gujolato.text.join_team","with":[{"translate":"gujolato.teams.yellow.name","color":"yellow"}]}

# Add to team
execute at @e[tag=level_selector,team=aqua] positioned ~-3 ~ ~-3 run team join aqua @a[dx=6,dy=-2,dz=6,gamemode=!creative]
execute at @e[tag=level_selector,team=black] positioned ~-3 ~ ~-3 run team join black @a[dx=6,dy=-2,dz=6,gamemode=!creative]
execute at @e[tag=level_selector,team=blue] positioned ~-3 ~ ~-3 run team join blue @a[dx=6,dy=-2,dz=6,gamemode=!creative]
execute at @e[tag=level_selector,team=cyan] positioned ~-3 ~ ~-3 run team join cyan @a[dx=6,dy=-2,dz=6,gamemode=!creative]
execute at @e[tag=level_selector,team=dark_blue] positioned ~-3 ~ ~-3 run team join dark_blue @a[dx=6,dy=-2,dz=6,gamemode=!creative]
execute at @e[tag=level_selector,team=gray] positioned ~-3 ~ ~-3 run team join gray @a[dx=6,dy=-2,dz=6,gamemode=!creative]
execute at @e[tag=level_selector,team=green] positioned ~-3 ~ ~-3 run team join green @a[dx=6,dy=-2,dz=6,gamemode=!creative]
execute at @e[tag=level_selector,team=purple] positioned ~-3 ~ ~-3 run team join purple @a[dx=6,dy=-2,dz=6,gamemode=!creative]
execute at @e[tag=level_selector,team=dark_red] positioned ~-3 ~ ~-3 run team join dark_red @a[dx=6,dy=-2,dz=6,gamemode=!creative]
execute at @e[tag=level_selector,team=gold] positioned ~-3 ~ ~-3 run team join gold @a[dx=6,dy=-2,dz=6,gamemode=!creative]
execute at @e[tag=level_selector,team=light_gray] positioned ~-3 ~ ~-3 run team join light_gray @a[dx=6,dy=-2,dz=6,gamemode=!creative]
execute at @e[tag=level_selector,team=lime] positioned ~-3 ~ ~-3 run team join lime @a[dx=6,dy=-2,dz=6,gamemode=!creative]
execute at @e[tag=level_selector,team=magenta] positioned ~-3 ~ ~-3 run team join magenta @a[dx=6,dy=-2,dz=6,gamemode=!creative]
execute at @e[tag=level_selector,team=red] positioned ~-3 ~ ~-3 run team join red @a[dx=6,dy=-2,dz=6,gamemode=!creative]
execute at @e[tag=level_selector,team=white] positioned ~-3 ~ ~-3 run team join white @a[dx=6,dy=-2,dz=6,gamemode=!creative]
execute at @e[tag=level_selector,team=yellow] positioned ~-3 ~ ~-3 run team join yellow @a[dx=6,dy=-2,dz=6,gamemode=!creative]

#execute at @e[tag=level_selector] ~-3 ~ ~-3 scoreboard players set @a[dx=6,dy=-2,dz=6,gamemode=!creative,score_level_min=1] reload 2