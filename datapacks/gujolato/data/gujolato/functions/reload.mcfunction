# The reload process is divided into multiple sections executed in the following order:
#1 reload function
#2a reloading function
#2b reloading game (solely for updating the game status)
#2c reloading function
#3 spawn function
#4a spawning function
#4b spawning game
#4c spawning function

# ++++++++++++++++++++ Reloading ++++++++++++++++++++
# 2 (#2c reloading function)
execute as @e[tag=level_creator,tag=reloading] at @s run function gujolato:reload/level_creator
execute as @a[tag=reloading] run function gujolato:reload/players
tag @e remove reloading

# 1 (#1 reload function)
execute if entity @e[tag=reload,team=aqua] run tag @e[team=aqua] add reloading
execute if entity @e[tag=reload,team=black] run tag @e[team=black] add reloading
execute if entity @e[tag=reload,team=blue] run tag @e[team=blue] add reloading
execute if entity @e[tag=reload,team=cyan] run tag @e[team=cyan] add reloading
execute if entity @e[tag=reload,team=dark_blue] run tag @e[team=dark_blue] add reloading
execute if entity @e[tag=reload,team=gray] run tag @e[team=gray] add reloading
execute if entity @e[tag=reload,team=green] run tag @e[team=green] add reloading
execute if entity @e[tag=reload,team=purple] run tag @e[team=purple] add reloading
execute if entity @e[tag=reload,team=dark_red] run tag @e[team=dark_red] add reloading
execute if entity @e[tag=reload,team=gold] run tag @e[team=gold] add reloading
execute if entity @e[tag=reload,team=light_gray] run tag @e[team=light_gray] add reloading
execute if entity @e[tag=reload,team=lime] run tag @e[team=lime] add reloading
execute if entity @e[tag=reload,team=magenta] run tag @e[team=magenta] add reloading
execute if entity @e[tag=reload,team=red] run tag @e[team=red] add reloading
execute if entity @e[tag=reload,team=white] run tag @e[team=white] add reloading
execute if entity @e[tag=reload,team=yellow] run tag @e[team=yellow] add reloading

tag @e[tag=reload] remove reload

# (#2a reloading function)
tellraw @a[tag=reloading] {"translate":"gujolato.text.level_loading"}
execute if entity @e[tag=reloading] run tellraw @a[tag=!reloading] {"translate":"gujolato.text.loading_lag"}
effect clear @a[tag=reloading]
effect give @a[tag=reloading] minecraft:instant_health 1 255 false
effect give @a[tag=reloading] minecraft:levitation 3 2 true
effect give @a[tag=reloading] minecraft:saturation 1 255 true
effect give @a[tag=reloading] minecraft:regeneration 1 255 true
effect give @a[tag=reloading] minecraft:resistance 1 7 true

fill ~ ~ ~ ~31 ~31 ~31 air

# ++++++++++++++++++++ Spawning ++++++++++++++++++++
# (#4c spawning function)
execute at @a[tag=spawning] run spawnpoint @s
tag @e[tag=spawning] remove spawning

# (#3 spawn function)
tag @e[tag=spawn] add spawning
tag @e[tag=spawn] remove spawn

# (#4a spawning function)
scoreboard players operation @a[tag=spawning,team=aqua] level = @e[tag=level_creator,team=aqua] level
scoreboard players operation @a[tag=spawning,team=black] level = @e[tag=level_creator,team=black] level
scoreboard players operation @a[tag=spawning,team=blue] level = @e[tag=level_creator,team=blue] level
scoreboard players operation @a[tag=spawning,team=cyan] level = @e[tag=level_creator,team=cyan] level
scoreboard players operation @a[tag=spawning,team=dark_blue] level = @e[tag=level_creator,team=dark_blue] level
scoreboard players operation @a[tag=spawning,team=gray] level = @e[tag=level_creator,team=gray] level
scoreboard players operation @a[tag=spawning,team=green] level = @e[tag=level_creator,team=green] level
scoreboard players operation @a[tag=spawning,team=purple] level = @e[tag=level_creator,team=purple] level
scoreboard players operation @a[tag=spawning,team=dark_red] level = @e[tag=level_creator,team=dark_red] level
scoreboard players operation @a[tag=spawning,team=gold] level = @e[tag=level_creator,team=gold] level
scoreboard players operation @a[tag=spawning,team=light_gray] level = @e[tag=level_creator,team=light_gray] level
scoreboard players operation @a[tag=spawning,team=lime] level = @e[tag=level_creator,team=lime] level
scoreboard players operation @a[tag=spawning,team=magenta] level = @e[tag=level_creator,team=magenta] level
scoreboard players operation @a[tag=spawning,team=red] level = @e[tag=level_creator,team=red] level
scoreboard players operation @a[tag=spawning,team=white] level = @e[tag=level_creator,team=white] level
scoreboard players operation @a[tag=spawning,team=yellow] level = @e[tag=level_creator,team=yellow] level