#2
#execute if @s[team=aqua] run tag @a[team=aqua] add reload
#...

#1
#execute if @s[team=aqua] run tag @e[tag=level_creator,team=aqua] add reload
#...


#tag remove @s reload

# Must be executed if

#2
execute as @e[tag=level_creator,tag=reloading] at @s run function gujolato:reload/level_creator
execute as @a[tag=reloading] run function gujolato:reload/players
tag @e remove reloading

#1
execute if entity @e[tag=reload,team=aqua] run tag @e[team=aqua] add reloading
execute if entity @e[tag=reload,team=black] run tag @e[team=black] add reloading
execute if entity @e[tag=reload,team=blue] run tag @e[team=blue] add reloading
execute if entity @e[tag=reload,team=cyan] run tag @e[team=cyan] add reloading
execute if entity @e[tag=reload,team=dark_blue] run tag @e[team=dark_blue] add reloading
execute if entity @e[tag=reload,team=gray] run tag @e[team=gray] add reloading
execute if entity @e[tag=reload,team=green] run tag @e[team=green] add reloading
execute if entity @e[tag=reload,team=purple] run tag @e[team=purple] add reloading
execute if entity @e[tag=reload,team=dark_red] run tag @e[team=dark_red] add reloading
execute if entity @e[tag=reload,team=gold] run tag @e[team=gold] add reloading
execute if entity @e[tag=reload,team=light_gray] run tag @e[team=light_gray] add reloading
execute if entity @e[tag=reload,team=lime] run tag @e[team=lime] add reloading
execute if entity @e[tag=reload,team=magenta] run tag @e[team=magenta] add reloading
execute if entity @e[tag=reload,team=red] run tag @e[team=red] add reloading
execute if entity @e[tag=reload,team=white] run tag @e[team=white] add reloading
execute if entity @e[tag=reload,team=yellow] run tag @e[team=yellow] add reloading
#...

tag @e[tag=reload] remove reload

tellraw @a[tag=reloading] {"translate":"gujolato.text.level_loading"}
execute if entity @e[tag=reloading] run tellraw @a[tag=!reloading] {"translate":"gujolato.text.loading_lag"}
effect clear @a[tag=reloading]
effect give @a[tag=reloading] minecraft:instant_health 1 255 false
effect give @a[tag=reloading] minecraft:levitation 3 2 true
effect give @a[tag=reloading] minecraft:saturation 1 255 true
effect give @a[tag=reloading] minecraft:regeneration 1 255 true
effect give @a[tag=reloading] minecraft:resistance 1 7 true

# spawning
tag @e[tag=spawning] remove spawning
tag @e[tag=spawn] add spawning
tag @e[tag=spawn] remove spawn

spawnpoint @a[tag=spawning]

scoreboard players operation @a[tag=spawn,team=aqua] level = @e[tag=levelCreator,team=aqua] level
scoreboard players operation @a[tag=spawn,team=black] level = @e[tag=levelCreator,team=black] level
scoreboard players operation @a[tag=spawn,team=blue] level = @e[tag=levelCreator,team=blue] level
scoreboard players operation @a[tag=spawn,team=cyan] level = @e[tag=levelCreator,team=cyan] level
scoreboard players operation @a[tag=spawn,team=dark_blue] level = @e[tag=levelCreator,team=dark_blue] level
scoreboard players operation @a[tag=spawn,team=gray] level = @e[tag=levelCreator,team=gray] level
scoreboard players operation @a[tag=spawn,team=green] level = @e[tag=levelCreator,team=green] level
scoreboard players operation @a[tag=spawn,team=purple] level = @e[tag=levelCreator,team=purple] level
scoreboard players operation @a[tag=spawn,team=dark_red] level = @e[tag=levelCreator,team=dark_red] level
scoreboard players operation @a[tag=spawn,team=gold] level = @e[tag=levelCreator,team=gold] level
scoreboard players operation @a[tag=spawn,team=light_gray] level = @e[tag=levelCreator,team=light_gray] level
scoreboard players operation @a[tag=spawn,team=lime] level = @e[tag=levelCreator,team=lime] level
scoreboard players operation @a[tag=spawn,team=magenta] level = @e[tag=levelCreator,team=magenta] level
scoreboard players operation @a[tag=spawn,team=red] level = @e[tag=levelCreator,team=red] level
scoreboard players operation @a[tag=spawn,team=white] level = @e[tag=levelCreator,team=white] level
scoreboard players operation @a[tag=spawn,team=yellow] level = @e[tag=levelCreator,team=yellow] level