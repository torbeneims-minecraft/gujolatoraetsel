# Show success
tellraw @a {"translate":"gujolato.text.success","with":[{"selector":"@s"},{"score":{"name":"@s","objective":"level"}}]}
# 1.13 workarout: Translated selectors don't update properly
#tellraw @a [{"translate":"gujolato.text.success.component0"},{"selector":"@s"},{"translate":"gujolato.text.success.component1"},{"score":{"name":"@s","objective":"level"}},{"translate":"gujolato.text.success.component2"}]
say success
execute at @s[type=player] run summon minecraft:firework_rocket ~ ~ ~

execute if entity @s[team=aqua] run scoreboard players add @e[team=aqua] level 1
execute if entity @s[team=black] run scoreboard players add @e[team=black] level 1
execute if entity @s[team=blue] run scoreboard players add @e[team=blue] level 1
execute if entity @s[team=cyan] run scoreboard players add @e[team=cyan] level 1
execute if entity @s[team=dark_blue] run scoreboard players add @e[team=dark_blue] level 1
execute if entity @s[team=gray] run scoreboard players add @e[team=gray] level 1
execute if entity @s[team=green] run scoreboard players add @e[team=green] level 1
execute if entity @s[team=purple] run scoreboard players add @e[team=purple] level 1
execute if entity @s[team=dark_red] run scoreboard players add @e[team=dark_red] level 1
execute if entity @s[team=gold] run scoreboard players add @e[team=gold] level 1
execute if entity @s[team=light_gray] run scoreboard players add @e[team=light_gray] level 1
execute if entity @s[team=lime] run scoreboard players add @e[team=lime] level 1
execute if entity @s[team=magenta] run scoreboard players add @e[team=magenta] level 1
execute if entity @s[team=red] run scoreboard players add @e[team=red] level 1
execute if entity @s[team=white] run scoreboard players add @e[team=white] level 1
execute if entity @s[team=yellow] run scoreboard players add @e[team=yellow] level 1

tag @s add reload
tag @s remove success